import { DialogHeader, DialogFooter, DialogContent, DialogTitle, DialogDescription, DialogClose } from "./ui/dialog";
import { Input } from "./ui/input";
import { Label } from "./ui/label";
import { Button } from "./ui/button";
import { useForm } from "react-hook-form";
import { z } from 'zod'
import { zodResolver } from '@hookform/resolvers/zod'

const createCompanySchema = z.object({
    cnpj: z.string(),
    socialReason: z.string(),
    address: z.string()
})

type CreateCompanySchema = z.infer<typeof createCompanySchema>

export function CreateCompanyDialog() {
    const { register, handleSubmit } = useForm<CreateCompanySchema>({
        resolver: zodResolver(createCompanySchema)
    })

    function handleCreateCompany(data: CreateCompanySchema) {
        console.log(data)
    }

    return (
        <DialogContent>
            <DialogHeader>
                <DialogTitle>Nova empresa</DialogTitle>
                <DialogDescription>Criar uma nova empresa no sistema</DialogDescription>
            </DialogHeader>

            <form onSubmit={handleSubmit(handleCreateCompany)} className="space-y-6">
                <div className="grid grid-cols-4 items-center text-right gap-2">
                    <Label htmlFor="cnpj">CNPJ</Label>
                    <Input className="col-span-3" {...register('cnpj')}></Input>
                </div>

                <div className="grid grid-cols-4 items-center text-right gap-2">
                    <Label htmlFor="socialReason">Razão Social</Label>
                    <Input className="col-span-3" {...register('socialReason')}></Input>
                </div>

                <div className="grid grid-cols-4 items-center text-right gap-2">
                    <Label htmlFor="address">Endereço</Label>
                    <Input className="col-span-3" {...register('address')}></Input>
                </div>

                <DialogFooter>
                    <DialogClose asChild>
                        <Button type="submit" variant={"outline"}>Cancelar</Button>
                    </DialogClose>
                    <Button type="submit">Salvar</Button>
                </DialogFooter>
            </form>
        </DialogContent>
    )
}