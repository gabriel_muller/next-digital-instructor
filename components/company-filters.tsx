import { Search } from "lucide-react";
import { Input } from "./ui/input";
import { Button } from "./ui/button";
import { useForm } from "react-hook-form";
import { z } from 'zod'
import { zodResolver } from '@hookform/resolvers/zod'

const companyFiltersSchema = z.object({
    cnpj: z.string(),
    socialReason: z.string()
})

type CompanyFiltersSchema = z.infer<typeof companyFiltersSchema>

export function CompanyFilters() {
    const { register, handleSubmit } = useForm<CompanyFiltersSchema>({
        resolver: zodResolver(companyFiltersSchema)
    })

    function handleFilterCompany(data: CompanyFiltersSchema) {
        console.log(data)
    }

    return (
        <form onSubmit={handleSubmit(handleFilterCompany)} className="flex items-center gap-2 my-4">
        <Input placeholder="CNPJ da empresa" {...register('cnpj')} />
        <Input placeholder="Razão social da empresa" {...register('socialReason')} />
        <Button type="submit" variant={"link"}>
          <Search className="w-4 h-4 mr-2" />
          Filtrar resultados
        </Button>
      </form>
    )
}