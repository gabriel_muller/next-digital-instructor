"use client"

import { Button } from '@/components/ui/button';
import { Axe, BarChart3, Building2, Fan, Fuel, Home, User } from 'lucide-react';
import React, { useState } from 'react';

interface SidebarProps {
  collapsed: boolean
}

export const Sidebar = ({ collapsed }: SidebarProps) => {
  return (
    <div className={`w-96 ${collapsed ? 'w-auto' : 'w-auto'}`}>

      <div className="p-6">
        <h2 className="text-white text-xl font-semibold mb-4">Menu</h2>
        <ul>
          <li className="mb-3">
            <Button type="submit" variant={"ghost"}>
              <Home className="w-5 h-5 mr-3" />
              {!collapsed && "Início"}
            </Button>
          </li>
          <li className="mb-3">
            <Button type="submit" variant={"ghost"}>
              <Fuel className="w-5 h-5 mr-3" />
              {!collapsed && "Consumo de combustível"}
            </Button>
          </li>
          <li className="mb-3">
            <Button type="submit" variant={"ghost"}>
              <BarChart3 className="w-5 h-5 mr-3" />
              {!collapsed && "Pontuação"}
            </Button>
          </li>
          <li className="mb-3">
            <Button type="submit" variant={"ghost"}>
              <Fan className="w-5 h-5 mr-3" />
              {!collapsed && "Emissão de gases"}
            </Button>
          </li>
          <li className="mb-3">
            <Button type="submit" variant={"ghost"}>
              <Building2 className="w-5 h-5 mr-3" />
              {!collapsed && "Empresas"}
            </Button>
          </li>
          <li className="mb-3">
            <Button type="submit" variant={"ghost"}>
              <Axe className="w-5 h-5 mr-3" />
              {!collapsed && "Equipamentos"}
            </Button>
          </li>
          <li className="mb-3">
            <Button type="submit" variant={"ghost"}>
              <User className="w-5 h-5 mr-3" />
              {!collapsed && "Usuários"}
            </Button>
          </li>
        </ul>
      </div>
    </div>
  );
};