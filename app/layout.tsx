import { Inter } from "next/font/google";
import "./globals.css";
import React, { ReactNode } from 'react';
import { Sidebar } from './sidebar';

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}

interface DefaultLayoutProps {
  children: ReactNode;
  sidebarCollapsed: boolean;
}

export function DefaultLayout({ children, sidebarCollapsed }: DefaultLayoutProps) {
  console.log(sidebarCollapsed)
  return (
    <div className="flex h-screen">
      <Sidebar collapsed={sidebarCollapsed} />
      <div className="flex flex-col w-full border-l #374151">
        {children}
      </div>
    </div>
  );
}