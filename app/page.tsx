"use client"

import { Button } from "@/components/ui/button";
import { Dialog, DialogClose, DialogContent, DialogDescription, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Table, TableBody, TableCell, TableFooter, TableHead, TableHeader, TableRow } from "@/components/ui/table";
import { Search, PlusCircle, Pencil, Trash, Eye, Menu, Download } from "lucide-react";
import { ThemeProvider } from "./themePrvider";
import { ModeToggle } from "./modeToggle";
import { DefaultLayout } from "./layout";
import React from "react";
import { Pagination, PaginationContent, PaginationItem, PaginationPrevious, PaginationLink, PaginationEllipsis, PaginationNext } from "@/components/ui/pagination";
import { Separator } from "@/components/ui/separator";
import { CompanyFilters } from "@/components/company-filters";
import { CreateCompanyDialog } from "@/components/create-company-dialog";

export default function Empresas() {
  const [sidebarCollapsed, setSidebarCollapsed] = React.useState(false);

  return (
    <DefaultLayout sidebarCollapsed={sidebarCollapsed}>
      <ThemeProvider attribute="class" defaultTheme="dark" enableSystem disableTransitionOnChange>
        <div className="p-1 max-w-full mx-6 space-y-4">
          <div className="flex items-center justify-between my-8">
            <Button variant="outline" size="icon" onClick={() => setSidebarCollapsed(!sidebarCollapsed)}>
              <Menu className="h-[1.2rem] w-[1.2rem] rotate-0 scale-100 transition-all dark:-rotate-90 dark:scale-0" />
              <Menu className="absolute h-[1.2rem] w-[1.2rem] rotate-90 scale-0 transition-all dark:rotate-0 dark:scale-100" />
              <span className="sr-only" />
            </Button>
            <ModeToggle />
          </div>
          <div className="flex my-2 items-center justify-between">
            <h1 className="text-3xl font-bold">Empresas</h1>
            <Dialog>
              <DialogTrigger asChild>
                <Button className="text-white bg-green-600 hover:bg-green-600/75">
                  <PlusCircle className="w-4 h-4 mr-2" />
                  Nova empresa
                </Button>
              </DialogTrigger>

              <CreateCompanyDialog />
            </Dialog>
          </div>
          <div className="flex items-center justify-between">
          <CompanyFilters/>
            <Button className="text-white bg-sky-600 hover:bg-sky-600/75">
              <Download className="w-4 h-4 mr-2" />
              Baixar CSV
            </Button>
          </div>

          <div className="border rounded-lg p-1">
            <div className="flex flex-col">
              <Table>
                <TableHeader>
                  <TableHead className="text-base font-bold">ID</TableHead>
                  <TableHead className="text-base font-bold">CNPJ</TableHead>
                  <TableHead className="text-base font-bold">Razão Social</TableHead>
                  <TableHead className="text-base font-bold w-1/3">Endereço</TableHead>
                  <TableHead className="text-base font-bold w-1/12">Ações</TableHead>
                </TableHeader>
                <TableBody>
                  {Array.from({ length: 5 }).map((_, i) => {
                    return (
                      <TableRow key={i}>
                        <TableCell>2419d4bb</TableCell>
                        <TableCell>55.309.414/0001-99</TableCell>
                        <TableCell>Gabriel Muller SA</TableCell>
                        <TableCell>Rua Carlos Cavalcante</TableCell>
                        <TableCell>
                          <div className="flex items-center gap-1">
                            <Eye className="w-7 h-7 p-1 border rounded-md text-white bg-teal-600 hover:bg-teal-600/75 cursor-pointer" />
                            <Pencil className="w-7 h-7 p-1 border rounded-md text-white bg-amber-600 hover:bg-amber-600/75 cursor-pointer" />
                            <Trash className="w-7 h-7 p-1 border rounded-md text-white bg-red-600 hover:bg-red-600/75 cursor-pointer" />
                          </div>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                </TableBody>
              </Table>
              <div>
                <Separator />
                <Pagination className="flex justify-end">
                  <PaginationContent>
                    <PaginationItem>
                      <PaginationPrevious href="#" />
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationLink href="#">1</PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationEllipsis />
                    </PaginationItem>
                    <PaginationItem>
                      <PaginationNext href="#" />
                    </PaginationItem>
                  </PaginationContent>
                </Pagination>
              </div>
            </div>
          </div>
        </div>
      </ThemeProvider>
    </DefaultLayout>
  );
}
